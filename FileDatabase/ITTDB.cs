using System;
using System.Collections.Generic;
using System.IO;
using FileDatabase;
using System.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace FileDatabase
{
    public class ITTDB
    {
        FileOperation file = new FileOperation();
        Serialization serializationObject = new Serialization();
        /// <summary>
        /// Method to do the save an item
        /// </summary>
        public int save(object data)
        {
            string fileName = file.getfileName(data);
            serializationObject.doSerialization(fileName, data);
            file.fileModifications(fileName);
            string fileData = File.ReadAllText("Student.json");
            return fileData.Length;
        }

        /// <summary>
        /// Method to find an element
        /// </summary>
        public dynamic find(Type dataType, dynamic data, string fieldType)
        {
            string fileName = dataType.Name + ".json";
            JArray fileData = serializationObject.doDeserialization(fileName);
            JObject dataForId = fileData.Children<JObject>().FirstOrDefault(o => o[fieldType].ToString() == data.ToString());
            string filedata = dataForId.ToString();
            if (filedata != null)
                return filedata;
            else
              throw new Exception("No Such data available");
        }

        /// <summary>
        /// Method to modify a element
        /// </summary>
        public bool modify(Type dataType, dynamic actualValue, dynamic modifyValue, string fieldType)
        {
            string fileName = dataType.Name + ".json";
            string fileData = File.ReadAllText(fileName);
            JArray fileContent = serializationObject.doDeserialization(fileName);
            JObject dataForField = fileContent.Children<JObject>().FirstOrDefault(o => o[fieldType].ToString() == actualValue.ToString());
            file.ModifyFile(fileData, actualValue, modifyValue, dataForField, fileName);
            return true;
        }

        
        /// <summary>
        /// Method to delete an item
        /// </summary>
        public int delete(Type dataType, dynamic data, string fieldType)
        {
            string fileName = dataType.Name + ".json";
            JArray fileData = serializationObject.doDeserialization(fileName);
            JObject dataForId = fileData.Children<JObject>().FirstOrDefault(o => o[fieldType].ToString() == data.ToString());
            string filedata = dataForId.ToString();
            file.checkForValidData(fileName, filedata);
            string fileContent = File.ReadAllText("Student.json");
            return fileContent.Length;
        }
    }
}
