using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;
using FileDatabase;
using System.Threading.Tasks;

namespace FileDatabase
{
    class Program
    {
        class Customer
        {

            public int id { set; get; }
            public String name { set; get; }
            public String address { set; get; }

            static void Main(string[] args)
            {
                 Customer customer = new Customer()
                {
                    id = 1,
                    address = "jPNagar",
                    name = "Deekshitha shetty",
                };
                
                List<Customer> customerList = new List<Customer>();
                customerList.Add(customer);
                ITTDB.save(customerList);
                var customers = ITTDB.find(typeof(Customer), 1,"id");
                ITTDB.modify(typeof(Customer), 1, 102,"id");
                ITTDB.delete(typeof(Customer), 1,"id");
            }
        }
    }
}



