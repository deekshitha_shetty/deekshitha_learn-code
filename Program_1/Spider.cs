using System;
using System.Collections;

namespace Queues

{
    public static class Spider
    {
        public static void Main()
        {

            int totalItems;
            int numberOfItemsNeeded;
            Queue queue = new Queue();
            int maximumOfSelectedItems = 0;
            int indexOfMaxValue = 0;
            int peekValueOfQueue;
            int tempData = 0;
            int minimumValue;

            int[] value = Array.ConvertAll(Console.ReadLine().Split(), int.Parse);
            totalItems = value[0];
            numberOfItemsNeeded = value[1];
            int leftItems = totalItems;
            bool[] isItemSelected = new bool[totalItems + 1];
            int[] data = Array.ConvertAll(Console.ReadLine().Trim().Split(), int.Parse);

          
            for (int index = 0; index < totalItems; index++)
            {
                queue.Enqueue(index);
                isItemSelected[index] = false;
            }

            for (int index = 0; index < numberOfItemsNeeded; index++)
            {
                tempData = 0;
                minimumValue = Math.Min(numberOfItemsNeeded, leftItems);
                maximumOfSelectedItems = -1;

                while (tempData < minimumValue)
                {
                    peekValueOfQueue = Convert.ToInt32(queue.Peek());

                    if (isItemSelected[peekValueOfQueue])
                    {
                        queue.Dequeue();
                        continue;
                    }

                    if (data[peekValueOfQueue] > maximumOfSelectedItems)
                    {
                        maximumOfSelectedItems = data[peekValueOfQueue];
                        indexOfMaxValue = peekValueOfQueue;
                    }

                    queue.Dequeue();
                    queue.Enqueue(peekValueOfQueue);
                    tempData++;

                    if (data[peekValueOfQueue] != 0)
                    {
                        data[peekValueOfQueue] -= 1;
                    }

                }

                isItemSelected[indexOfMaxValue] = true;
                Console.Write(indexOfMaxValue + 1);
                Console.Write(" ");
                leftItems--;

            }
            Console.ReadKey();
        }

    }
}